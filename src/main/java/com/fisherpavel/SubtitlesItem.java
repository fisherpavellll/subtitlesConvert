package com.fisherpavel;

public class SubtitlesItem {
    private String startTime;
    private String endTime;
    private String text;

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "SubtitlesItem{" +
                "startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}