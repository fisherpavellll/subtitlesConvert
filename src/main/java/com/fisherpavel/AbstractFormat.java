package com.fisherpavel;

import java.io.*;

public abstract class AbstractFormat implements Operations{

    @Override
    public abstract void convert(String param);

    @Override
    public abstract void moveTime(String param);

    public File getFile(String path){
        return new File(path);
    }

    public void readFile(String path){

        try (Reader reader = new BufferedReader(new FileReader(path))){

            int c;
            while (( c =reader.read()) != -1){
                System.out.print((char) c);
            }

        } catch (IOException ex){
            ex.getMessage();
        }
    }
}
