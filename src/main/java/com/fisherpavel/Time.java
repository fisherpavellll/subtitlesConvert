package com.fisherpavel;

public class Time {
    private  int h, m, s, ms;

    public Time(String format, String value){
        if(format.equalsIgnoreCase("hh:mm:ss,ms")){ // .srt

            h = Integer.parseInt(value.substring(0, 2));
            m = Integer.parseInt(value.substring(3, 5));
            s = Integer.parseInt(value.substring(6, 8));
            ms = Integer.parseInt(value.substring(9, 12));
        }
    }

    public int getH() {
        return h;
    }

    public int getM() {
        return m;
    }

    public int getMs() {
        return ms;
    }

    public int getS() {
        return s;
    }

    public void setH(int h) {
        this.h = h;
    }

    public void setM(int m) {
        this.m = m;
    }

    public void setMs(int ms) {
        this.ms = ms;
    }

    public void setS(int s) {
        this.s = s;
    }

    public String getTime(String format){

        StringBuilder time = new StringBuilder();
        String aux;

        if(format.equalsIgnoreCase("hh:mm:ss,ms")){
            aux = String.valueOf(this.h);
            if(this.h < 10) time.append("0");
            time.append(aux);
            time.append(":");

            aux = String.valueOf(this.m);
            if(this.m < 10) time.append("0");
            time.append(aux);
            time.append(":");

            aux = String.valueOf(this.s);
            if(this.s < 10) time.append("0");
            time.append(aux);
            time.append(",");

            aux = String.valueOf(this.ms);
            if(this.ms < 100) time.append("0");
            else if(this.ms < 10) time.append("00");
            time.append(aux);
        }
        return time.toString();
    }
}
