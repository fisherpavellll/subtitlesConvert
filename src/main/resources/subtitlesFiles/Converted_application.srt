1
00:00:10.680 --> 00:00:14.930
Welcome to the first lecture where I'm going to introduce you to this desk management application.

2
00:00:14.940 --> 00:00:18.170
We're going to build together though.

3
00:00:18.200 --> 00:00:23.420
Let me talk about the application structure in the long term though by long term I mean by the time

4
00:00:23.420 --> 00:00:28.420
we finish this entire Of course what we're going to have we're going to have a structure where we have

5
00:00:28.570 --> 00:00:29.440
an app module.

6
00:00:29.440 --> 00:00:32.050
So this is the root module for the application.

7
00:00:32.230 --> 00:00:37.600
Then we're going to start by implementing the tasks module so anything to do with tasks we're gonna

8
00:00:37.600 --> 00:00:39.720
implement a controller and a service.

9
00:00:39.720 --> 00:00:44.590
The controller is going to hand the incoming HP requests communicate with the service.

10
00:00:44.590 --> 00:00:48.280
Some point we're going to introduce status validation.

11
00:00:48.430 --> 00:00:52.750
We're going to introduce a task entity and desk repository so at this point we're going to introduce

12
00:00:52.750 --> 00:00:59.650
data persistence using type or RAM which is a very popular and very demanded technology to work with.

13
00:00:59.800 --> 00:01:05.830
And of course there is going to be a lot more to it and we're going to work on the auto module for authentication

14
00:01:05.830 --> 00:01:07.300
and authorization.

15
00:01:07.480 --> 00:01:13.180
We're going to introduce a controller and a service as well and we're going to create the user entity

16
00:01:13.180 --> 00:01:16.500
in user stories so we're going to start working with a database as well.

17
00:01:16.590 --> 00:01:19.920
We're going to be able to create users.

18
00:01:20.040 --> 00:01:25.950
We're going to introduce a distributed strategy so a way for us to authenticate users and authorize

19
00:01:25.950 --> 00:01:32.090
users using JWT tokens which is an incredibly common concept these days.

20
00:01:32.340 --> 00:01:35.120
And of course we're going to have a lot more than at the end.

21
00:01:35.130 --> 00:01:38.030
We're going to have communication between these two modules.

22
00:01:38.100 --> 00:01:43.380
It's like in a real world application where users are going to be able to create tasks and own their

23
00:01:43.380 --> 00:01:47.430
own tasks or delete their own tasks update get their own tasks.

24
00:01:47.430 --> 00:01:53.690
Obviously users are going to have task ownership and that's the annual we're going to do the rest api

25
00:01:53.690 --> 00:01:58.520
is here and here are the endpoints are we're going to have four tasks we're going to be able to get

26
00:01:58.520 --> 00:01:59.450
old tasks.

27
00:01:59.450 --> 00:02:04.370
We're also going to be able to get tasks with filters so filter by desk status or the description in

28
00:02:04.370 --> 00:02:10.550
the title we're gonna be able to get an individual task you're gonna be able to create a task you're

29
00:02:10.580 --> 00:02:14.150
gonna be able to lead a task and update the task status.

30
00:02:14.300 --> 00:02:20.140
These are all full CRUD operations and then for authentication we're going to be able to sign up and

31
00:02:20.140 --> 00:02:26.950
sign in and here are the objectives in terms of next yes you're going to learn how to properly work

32
00:02:26.950 --> 00:02:33.040
with Nest yes modules module modular applications you're gonna be able to create controllers that to

33
00:02:33.050 --> 00:02:39.450
work within these modules and handle HP requests you're gonna be able to create services where you contain

34
00:02:39.450 --> 00:02:45.510
your business logic you're going to put a form controller to service communication and you're going

35
00:02:45.510 --> 00:02:50.780
to learn how to apply validation using USGS pipes you're going to create your own custom pipes and also

36
00:02:50.780 --> 00:02:55.460
use the built in validation pipe and pass it by coming with Nesta yes

37
00:02:59.570 --> 00:03:04.810
in terms of Beck and general architectures our objectives are to develop a production ready rest api

38
00:03:04.840 --> 00:03:10.400
is you're going to learn how to support CRUD operations in your API so create read update and delete

39
00:03:11.540 --> 00:03:17.210
you're going to learn how to properly apply error handling out to use data transfer objects or details

40
00:03:17.210 --> 00:03:21.060
which is a very common pattern in software development in general.

41
00:03:21.110 --> 00:03:26
You're going to learn how to apply appropriate system modularity so you'll be able to have super modular

42
00:03:26 --> 00:03:32.120
systems that can just be taken and then ported to a separate micros service in the future or easily

43
00:03:32.210 --> 00:03:37.180
maintained you're going to learn the best practices of back in development.

44
00:03:37.260 --> 00:03:42.380
You're going to learn how to properly manage your configuration in an application for different environments

45
00:03:42.770 --> 00:03:48.140
the development setup or production setup you're going to learn how to apply proper logging for different

46
00:03:48.140 --> 00:03:49.820
environments as well.

47
00:03:49.990 --> 00:03:51.920
You're going to learn security best practices

48
00:03:55.260 --> 00:03:59.550
in terms of data persistence you're going to learn how to connect your application to a database you're

49
00:03:59.550 --> 00:04:05.220
going to learn how to work with a relational database you're going to use type all RAM which is a very

50
00:04:05.310 --> 00:04:11.630
demanded technology these days you're going to learn how to use the query builder to write simple and

51
00:04:11.630 --> 00:04:12.660
complex queries.

52
00:04:12.680 --> 00:04:20.190
This is a super super useful and amazing tool and you're going to learn very very important insights

53
00:04:20.310 --> 00:04:25.050
as we develop the applications on how important performance is when working with a database and how

54
00:04:25.050 --> 00:04:30.260
to properly think about the performance aspect as you develop your application and get the best result.

55
00:04:31.730 --> 00:04:36.860
In terms of authorization and authentication you're going to learn how to apply the sign up and sign

56
00:04:36.890 --> 00:04:41.280
an operations authentication and authorization of resources.

57
00:04:41.360 --> 00:04:47.970
You're going to have protected resources so only users will be able to handle their own tasks you're

58
00:04:47.970 --> 00:04:53.520
going to learn how to apply utility tokens or Jason web Dawkins as they're called in your applications

59
00:04:54.830 --> 00:04:58.730
you're going to learn how to hash passwords using cryptographic libraries.

60
00:04:58.770 --> 00:05:03.800
You're going to learn how to apply salts and how to properly store passwords in a very secure way in

61
00:05:03.800 --> 00:05:07.560
a database so you wouldn't have to worry about data breaches or being attacked

62
00:05:10.390 --> 00:05:14.590
you're going to learn how to polish your obligation before releasing you to production.

63
00:05:14.590 --> 00:05:15.850
You're going to learn how to deploy.

64
00:05:15.920 --> 00:05:19.740
Yes applications UAW us or Amazon Web Services.

65
00:05:19.960 --> 00:05:23.120
You're going to learn how to deploy a front end application.

66
00:05:23.200 --> 00:05:28.840
Amazon has 3 4 static web hosting and you're going to learn finally how to wire up the front end and

67
00:05:28.840 --> 00:05:34.920
the back and off your application and then as a bonus I'm gonna provide you with a free fully featured

68
00:05:34.920 --> 00:05:38.850
front and application that consumes the API that you two are going to develop throughout the course

69
00:05:38.970 --> 00:05:43.330
for your own use though I hope you're ready and I hope you're excited.

70
00:05:43.330 --> 00:05:44.470
Now let's begin.

