package com.fisherpavel.formats;

import com.fisherpavel.AbstractFormat;
import com.fisherpavel.SubtitlesItem;
import java.io.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SrtFormat extends AbstractFormat {

    private String path;
    private File file;

    private List<SubtitlesItem> subtitlesItems = new ArrayList<>();

    public SrtFormat(String path){
        this.path = path;
        this.file = getFile(path);
    }

    @Override
    public void convert(String param) {
        int time = Integer.parseInt(param);
    }

    @Override
    public void moveTime(String sec) {

            String convertPath = convertPath();

            addToList();
            int count = 1;
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(convertPath))){

                for(SubtitlesItem item: this.subtitlesItems){

                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss,SSS");
                    LocalTime convertStart = LocalTime.parse(item.getStartTime(), formatter)
                            .plusSeconds(Integer.valueOf(sec));
                    LocalTime convertEnd = LocalTime.parse(item.getEndTime(), formatter)
                            .plusSeconds(Integer.valueOf(sec));

                    String convertedStart = convertStart.toString();
                    String convertedEnd = convertEnd.toString();

                    writer.write(String.valueOf(count));
                    writer.write('\n');
                    String t = String.format("%s --> %s\n", convertedStart, convertedEnd);
                    writer.write(t);
                    writer.write(item.getText() + '\n');
                    writer.write('\n');
                    count++;
                }

            } catch (IOException ex){
                ex.getMessage();
            }
    }

    private String convertPath(){
        String convertName = String.format("Converted_%s", this.file.getName());
        return String.format("%s%s%s", this.file.getParent(), File.separator, convertName);
    }

    private void addToList(){

        try (BufferedReader reader = new BufferedReader(new FileReader(path))){

            int count = 2;
            String line;
            SubtitlesItem subtitlesItem = new SubtitlesItem();
            while ((line = reader.readLine()) != null){

                if(count % 2 != 0 && !line.isEmpty()){
                    subtitlesItem.setStartTime(line.substring(0, 12));
                    subtitlesItem.setEndTime(line.substring(17, 29));
                }
                if(count % 2 == 0 && count % 4 == 0 && !line.isEmpty()) {
                    subtitlesItem.setText(line);
                    this.subtitlesItems.add(subtitlesItem);
                    subtitlesItem = new SubtitlesItem();
                }
                count++;
            }

        } catch (IOException ex){
            ex.getMessage();
        }
    }
}