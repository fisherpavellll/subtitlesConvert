package com.fisherpavel.formats;

import com.fisherpavel.AbstractFormat;

import java.io.File;

public class TxtFormat extends AbstractFormat {

    private String path;
    private File file;


    public TxtFormat(String path){
        this.path = path;
        this.file = getFile(path);
    }

    @Override
    public void convert(String param) {
        System.out.printf("File exists: %b\n", file.exists());
    }

    @Override
    public void moveTime(String param) {
        int par = Integer.parseInt(param);
        System.out.println("move time");
        System.out.println(par);
    }
}
