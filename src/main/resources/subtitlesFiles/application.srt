1
00:00:00,680 --> 00:00:04,930
Welcome to the first lecture where I'm going to introduce you to this desk management application.

2
00:00:04,940 --> 00:00:08,170
We're going to build together though.

3
00:00:08,200 --> 00:00:13,420
Let me talk about the application structure in the long term though by long term I mean by the time

4
00:00:13,420 --> 00:00:18,420
we finish this entire Of course what we're going to have we're going to have a structure where we have

5
00:00:18,570 --> 00:00:19,440
an app module.

6
00:00:19,440 --> 00:00:22,050
So this is the root module for the application.

7
00:00:22,230 --> 00:00:27,600
Then we're going to start by implementing the tasks module so anything to do with tasks we're gonna

8
00:00:27,600 --> 00:00:29,720
implement a controller and a service.

9
00:00:29,720 --> 00:00:34,590
The controller is going to hand the incoming HP requests communicate with the service.

10
00:00:34,590 --> 00:00:38,280
Some point we're going to introduce status validation.

11
00:00:38,430 --> 00:00:42,750
We're going to introduce a task entity and desk repository so at this point we're going to introduce

12
00:00:42,750 --> 00:00:49,650
data persistence using type or RAM which is a very popular and very demanded technology to work with.

13
00:00:49,800 --> 00:00:55,830
And of course there is going to be a lot more to it and we're going to work on the auto module for authentication

14
00:00:55,830 --> 00:00:57,300
and authorization.

15
00:00:57,480 --> 00:01:03,180
We're going to introduce a controller and a service as well and we're going to create the user entity

16
00:01:03,180 --> 00:01:06,500
in user stories so we're going to start working with a database as well.

17
00:01:06,590 --> 00:01:09,920
We're going to be able to create users.

18
00:01:10,040 --> 00:01:15,950
We're going to introduce a distributed strategy so a way for us to authenticate users and authorize

19
00:01:15,950 --> 00:01:22,090
users using JWT tokens which is an incredibly common concept these days.

20
00:01:22,340 --> 00:01:25,120
And of course we're going to have a lot more than at the end.

21
00:01:25,130 --> 00:01:28,030
We're going to have communication between these two modules.

22
00:01:28,100 --> 00:01:33,380
It's like in a real world application where users are going to be able to create tasks and own their

23
00:01:33,380 --> 00:01:37,430
own tasks or delete their own tasks update get their own tasks.

24
00:01:37,430 --> 00:01:43,690
Obviously users are going to have task ownership and that's the annual we're going to do the rest api

25
00:01:43,690 --> 00:01:48,520
is here and here are the endpoints are we're going to have four tasks we're going to be able to get

26
00:01:48,520 --> 00:01:49,450
old tasks.

27
00:01:49,450 --> 00:01:54,370
We're also going to be able to get tasks with filters so filter by desk status or the description in

28
00:01:54,370 --> 00:02:00,550
the title we're gonna be able to get an individual task you're gonna be able to create a task you're

29
00:02:00,580 --> 00:02:04,150
gonna be able to lead a task and update the task status.

30
00:02:04,300 --> 00:02:10,140
These are all full CRUD operations and then for authentication we're going to be able to sign up and

31
00:02:10,140 --> 00:02:16,950
sign in and here are the objectives in terms of next yes you're going to learn how to properly work

32
00:02:16,950 --> 00:02:23,040
with Nest yes modules module modular applications you're gonna be able to create controllers that to

33
00:02:23,050 --> 00:02:29,450
work within these modules and handle HP requests you're gonna be able to create services where you contain

34
00:02:29,450 --> 00:02:35,510
your business logic you're going to put a form controller to service communication and you're going

35
00:02:35,510 --> 00:02:40,780
to learn how to apply validation using USGS pipes you're going to create your own custom pipes and also

36
00:02:40,780 --> 00:02:45,460
use the built in validation pipe and pass it by coming with Nesta yes

37
00:02:49,570 --> 00:02:54,810
in terms of Beck and general architectures our objectives are to develop a production ready rest api

38
00:02:54,840 --> 00:03:00,400
is you're going to learn how to support CRUD operations in your API so create read update and delete

39
00:03:01,540 --> 00:03:07,210
you're going to learn how to properly apply error handling out to use data transfer objects or details

40
00:03:07,210 --> 00:03:11,060
which is a very common pattern in software development in general.

41
00:03:11,110 --> 00:03:16,000
You're going to learn how to apply appropriate system modularity so you'll be able to have super modular

42
00:03:16,000 --> 00:03:22,120
systems that can just be taken and then ported to a separate micros service in the future or easily

43
00:03:22,210 --> 00:03:27,180
maintained you're going to learn the best practices of back in development.

44
00:03:27,260 --> 00:03:32,380
You're going to learn how to properly manage your configuration in an application for different environments

45
00:03:32,770 --> 00:03:38,140
the development setup or production setup you're going to learn how to apply proper logging for different

46
00:03:38,140 --> 00:03:39,820
environments as well.

47
00:03:39,990 --> 00:03:41,920
You're going to learn security best practices

48
00:03:45,260 --> 00:03:49,550
in terms of data persistence you're going to learn how to connect your application to a database you're

49
00:03:49,550 --> 00:03:55,220
going to learn how to work with a relational database you're going to use type all RAM which is a very

50
00:03:55,310 --> 00:04:01,630
demanded technology these days you're going to learn how to use the query builder to write simple and

51
00:04:01,630 --> 00:04:02,660
complex queries.

52
00:04:02,680 --> 00:04:10,190
This is a super super useful and amazing tool and you're going to learn very very important insights

53
00:04:10,310 --> 00:04:15,050
as we develop the applications on how important performance is when working with a database and how

54
00:04:15,050 --> 00:04:20,260
to properly think about the performance aspect as you develop your application and get the best result.

55
00:04:21,730 --> 00:04:26,860
In terms of authorization and authentication you're going to learn how to apply the sign up and sign

56
00:04:26,890 --> 00:04:31,280
an operations authentication and authorization of resources.

57
00:04:31,360 --> 00:04:37,970
You're going to have protected resources so only users will be able to handle their own tasks you're

58
00:04:37,970 --> 00:04:43,520
going to learn how to apply utility tokens or Jason web Dawkins as they're called in your applications

59
00:04:44,830 --> 00:04:48,730
you're going to learn how to hash passwords using cryptographic libraries.

60
00:04:48,770 --> 00:04:53,800
You're going to learn how to apply salts and how to properly store passwords in a very secure way in

61
00:04:53,800 --> 00:04:57,560
a database so you wouldn't have to worry about data breaches or being attacked

62
00:05:00,390 --> 00:05:04,590
you're going to learn how to polish your obligation before releasing you to production.

63
00:05:04,590 --> 00:05:05,850
You're going to learn how to deploy.

64
00:05:05,920 --> 00:05:09,740
Yes applications UAW us or Amazon Web Services.

65
00:05:09,960 --> 00:05:13,120
You're going to learn how to deploy a front end application.

66
00:05:13,200 --> 00:05:18,840
Amazon has 3 4 static web hosting and you're going to learn finally how to wire up the front end and

67
00:05:18,840 --> 00:05:24,920
the back and off your application and then as a bonus I'm gonna provide you with a free fully featured

68
00:05:24,920 --> 00:05:28,850
front and application that consumes the API that you two are going to develop throughout the course

69
00:05:28,970 --> 00:05:33,330
for your own use though I hope you're ready and I hope you're excited.

70
00:05:33,330 --> 00:05:34,470
Now let's begin.
