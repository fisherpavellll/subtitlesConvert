package com.fisherpavel;

import com.fisherpavel.formats.AccFormat;
import com.fisherpavel.formats.SrtFormat;
import com.fisherpavel.formats.TxtFormat;

public class FormatFactory {

    public AbstractFormat createFormat(Formats type, String path){
        AbstractFormat format = null;

        switch (type){
            case TXT -> {
                format = new TxtFormat(path);
                break;
            }
            case ACC -> {
                format = new AccFormat(path);
                break;
            }
            case SRT -> {
                format = new SrtFormat(path);
                break;
            }
            default -> {
                System.out.println("mimo");            }
        }

        return format;
    }
}
