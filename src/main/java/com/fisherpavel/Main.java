package com.fisherpavel;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String path = scanner.nextLine();   // "./src/main/resources/subtitlesFiles/application.srt"
        String operation = scanner.nextLine();
        String parameter = scanner.nextLine();
        scanner.close();

        FormatFactory formatFactory = new FormatFactory();
        AbstractFormat file = formatFactory.createFormat(converter(path), path);

        executeOperation(path, operation, parameter, file);
    }

    public static void executeOperation(String path, String operation, String parameter, AbstractFormat file) throws Exception {
        if(operation.equals("move time")){
            try {
                Integer param = Integer.parseInt(parameter);

            } catch (Exception ex){
                throw new Exception("invalid parameter");
            }
            file.moveTime(parameter);


        } else if(operation.equals("convert")){
            if(!parameter.equals("txt") && !parameter.equals("srt") && !parameter.equals("acc") ){
                throw new Exception("Invalid format");
            }
            file.convert(parameter);
            file.readFile(path);
        } else {
            throw new Exception("Operation not found");
        }
    }

    public static Formats converter(String path) throws Exception {

        if(path.endsWith("txt")){
            return Formats.TXT;
        } else if(path.endsWith(".acc")){
            return Formats.ACC;
        } else if(path.endsWith(".srt")){
            return Formats.SRT;
        } else {
            throw new Exception("Invalid format");
        }
    }
}
