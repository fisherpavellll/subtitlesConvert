package com.fisherpavel;

public interface Operations {
     void convert(String param);
     void moveTime(String param);
}
