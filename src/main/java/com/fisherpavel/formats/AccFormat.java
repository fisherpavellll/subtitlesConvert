package com.fisherpavel.formats;

import com.fisherpavel.AbstractFormat;

import java.io.File;

public class AccFormat extends AbstractFormat {

    private String path;
    private File file;

    public AccFormat(String path){
        this.path = path;
        this.file = getFile(path);
    }

    @Override
    public void convert(String param) {

    }

    @Override
    public void moveTime(String param) {

    }
}
